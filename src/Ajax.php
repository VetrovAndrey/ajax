<?php

namespace Vetrov;

use Vetrov\Collections\Actions as ActionsCollection;
use Vetrov\Exceptions\ActionException;

class Ajax
{
    private static ?ActionsCollection $actions = null;

    public static function action(string $action, callable $handler): void
    {
        if (is_null(static::$actions) || ! (static::$actions instanceof ActionsCollection)) {
            static::$actions = new ActionsCollection();
        }

        if (static::$actions->has($action)) {
            throw new ActionException("Action with key \"{$action}\" already exists");
        }

        static::$actions->put($action, $handler);
    }

    public static function handle($action, ?array $data = null)
    {
        if (static::$actions->has($action)) {
            return call_user_func(static::$actions->get($action), $data);
        } else {
            return Response::fail("Action with key \"$action\" is undefined", [], 501);
        }
    }

    public static function execute(?string $action = null, ?array $data = null): void
    {
        try {
            ob_start();
            $result = static::handle(
                $action ?? $_REQUEST['action'],
                $data ?? $_REQUEST['data']
            );

            if (! ($result instanceof Response)) {
                $result = ob_get_clean();
            }
        } catch (\Exception $e) {
            $result = Response::fail($e->getMessage());
        }

        echo ($result instanceof Response) ? $result->get() : $result;
    }
}