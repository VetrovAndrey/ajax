<?php

namespace Vetrov;

class Response
{
    private $data;
    private $status;
    private $message;

    public function __construct($data, $status = 200)
    {
        $this->data = $data;
        $this->status = $status;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
        return $this;
    }

    public static function success($data, int $status = 200)
    {
        return new static($data, $status);
    }

    public static function fail($message, array $details = [], $status = 400)
    {
        return (new static($details, $status))->setMessage($message);
    }

    public function get()
    {
        if ($this->status !== 200) {
            http_response_code($this->status);
        }

        return json_encode($this->status >= 400 ? ['message' => $this->message, 'details' => $this->data] : $this->data);
    }
}