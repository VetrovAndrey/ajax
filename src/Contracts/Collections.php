<?php

namespace Vetrov\Contracts;

use Vetrov\Exceptions\CollectionException;

abstract class Collections
{
    protected $items = [];

    public function put($key,  $value)
    {
        $this->items[$key] = $value;
    }

    public function push($value)
    {
        $this->items[] = $value;
    }

    public function has(...$keys): bool
    {
        return $keys == array_filter($keys, function ($value) {
                return array_key_exists($value, $this->items);
            });
    }

    public function get($key)
    {
        return $this->items[$key];
    }

    public function __get($name): mixed
    {
        if ($this->has($name)) {
            return $this->items[$name];
        }

        throw new CollectionException("value with key \"{$name}\" is not defined");
    }
}